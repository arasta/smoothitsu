insert into component(id, amount, brand, color_hex, kcal_per_unit, name, unit, unit_price_eur)
values (2001, 2, 'Eesti', 'red', 23, 'Maasikas', 'kg', 3);

insert into component(id, amount, brand, color_hex, kcal_per_unit, name, unit, unit_price_eur)
values (2002, 6, 'Apple', 'red', 20, 'Õun', 'kg', 1);

insert into component(id, amount, brand, color_hex, kcal_per_unit, name, unit, unit_price_eur)
values (2003, 10, 'Berry', 'blue', 12, 'Mustikas', 'kg', 1);

insert into component(id, amount, brand, color_hex, kcal_per_unit, name, unit, unit_price_eur)
values (2004, 7, 'Fruits', 'yellow', 23, 'Banaan', 'kg', 2);

insert into component(id, amount, brand, color_hex, kcal_per_unit, name, unit, unit_price_eur)
values (2005, 7, 'Alma', 'white', 23, 'Piim', 'l', 1);

insert into smoothie(id, calories, description, image, instructions, name, price, unit, unit_value)
values (2001, 200, 'väga hea smuuti', 'path/to/pic', 'sega kõik asjad kokku', 'ParimSmuuti', 3, 'klaas', 1);

insert into smoothie_components(smoothie_id, component_id) values (2001, 2002);
insert into smoothie_components(smoothie_id, component_id) values (2001, 2004);
insert into smoothie_components(smoothie_id, component_id) values (2001, 2005);