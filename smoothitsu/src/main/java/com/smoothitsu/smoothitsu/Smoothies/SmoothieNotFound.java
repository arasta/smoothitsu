package com.smoothitsu.smoothitsu.Smoothies;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
public class SmoothieNotFound extends RuntimeException {
    public SmoothieNotFound(String smoothie_not_found) {
        super(smoothie_not_found);
    }
}