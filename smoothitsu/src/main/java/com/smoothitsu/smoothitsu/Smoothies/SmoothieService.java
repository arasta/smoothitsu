package com.smoothitsu.smoothitsu.Smoothies;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class SmoothieService {

    @Autowired
    private SmoothieRepository smoothieRepository;

    public Iterable<Smoothie> getSmoothies(){
        return smoothieRepository.findAll();
    }

    public Smoothie getSmoothieById(Long id){
        Optional<Smoothie> byId = smoothieRepository.findById(id);
        if(byId.isPresent()){
            return byId.get();
        }else{
            String error_mes = "Smoothie not found";
            log.error(error_mes);
            throw new SmoothieNotFound(error_mes);
        }
    }

    public Smoothie saveSmoothie(Smoothie smoothie){
        return smoothieRepository.save(smoothie);
    }


}
