package com.smoothitsu.smoothitsu.Smoothies;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmoothieRepository extends PagingAndSortingRepository<Smoothie, Long> {
}
