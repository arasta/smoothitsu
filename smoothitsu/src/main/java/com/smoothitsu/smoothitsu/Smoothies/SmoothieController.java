package com.smoothitsu.smoothitsu.Smoothies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/smoothies")
public class SmoothieController {

    @Autowired
    private SmoothieService smoothieService;

    @GetMapping
    private Iterable<Smoothie> getComponents() {
        return smoothieService.getSmoothies();
    }

    @GetMapping(path = "/{id}")
    private Smoothie getSmoothie(@PathVariable Long id){
        return smoothieService.getSmoothieById(id);
    }

    @PostMapping
    private Smoothie saveSmoothie(@RequestBody Smoothie smoothie){
        return smoothieService.saveSmoothie(smoothie);
    }
}
