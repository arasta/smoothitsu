package com.smoothitsu.smoothitsu.Smoothies;

import com.smoothitsu.smoothitsu.Components.Component;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Smoothie {
    @Id
    @GeneratedValue
    private Long id;
    private Long calories;
    private String description;
    private String name;
    private Long price;
    private String unit;
    private Long unitValue;
    private String image;
    private String instructions;

    @ManyToMany
    @JoinTable(
            name = "smoothie_components",
            joinColumns = @JoinColumn(name="smoothie_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "component_id", referencedColumnName = "id")
    )
    private Set<Component> smoothieComponents;
}
