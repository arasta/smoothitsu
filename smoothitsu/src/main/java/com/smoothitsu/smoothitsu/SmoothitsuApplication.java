package com.smoothitsu.smoothitsu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmoothitsuApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmoothitsuApplication.class, args);
	}
}
