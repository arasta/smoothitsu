package com.smoothitsu.smoothitsu.Components;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Component {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String brand;
    private Long kcalPerUnit;
    private Long amount;
    private String unit;
    private Long unitPriceEur;
    private String colorHex;
}
