package com.smoothitsu.smoothitsu.Components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComponentService {

    @Autowired
    private ComponentRepository componentRepository;

    public Iterable<Component> getComponents(){
        return componentRepository.findAll();
    }

    public Component saveComponent(Component component){
        return componentRepository.save(component);
    }
}
