package com.smoothitsu.smoothitsu.Components;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentRepository extends PagingAndSortingRepository<Component, Long> {
}
