package com.smoothitsu.smoothitsu.Components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/components")
public class ComponentController {

    @Autowired
    private ComponentService componentService;

    @GetMapping
    private Iterable<Component> getComponents() {
        return componentService.getComponents();
    }

    @PostMapping
    private Component saveComponent(@RequestBody Component component){
        return componentService.saveComponent(component);
    }



    //@GetMapping(path = "/nr")
    //private int getNumber(){
    //    return 5 + 2;
    //}

}
